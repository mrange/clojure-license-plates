(ns clojure-license-plates.core
  (:require [clojure.java.io :as io]
            [criterium.core :as crit]
            [clj-mmap :as mmap])
  (:import (mxy IntTools)
           (java.util BitSet))
  (:gen-class))

;; functions to generate test files
(def alphabet-length 26)

(def alphabet
  (remove #{\I \Q \V} (mapv (comp char (partial + 65)) (range alphabet-length))))

(defn random-string
  "Returns a random string of specified length"
  [length]
  (apply str (take length (repeatedly #(rand-nth alphabet)))))

(defn random-number
  "Returns a random string with numbers 0-9"
  [length]
  (apply str (take length (repeatedly #(int (rand 10))))))

(defn license-plate-seq
  [list-length string-length]
  (take list-length (repeatedly #(str
                                  (random-string string-length)
                                  (random-number string-length)))))

(defn random-license-plate []
  (str (random-string 3) (random-number 3)))

(defn create-sample-file [file-name number-of-items]
  (spit file-name
        (with-out-str (dotimes [n number-of-items]
                        (println (random-license-plate))))))

(defn read-sample-file [file-name]
  (letfn [(helper [rdr]
            (lazy-seq
              (if-let [line (.readLine rdr)]
                (cons line (helper rdr))
                (do (.close rdr) nil))))]
    (helper )))

;; functions to read file to byte array
(defn file-to-byte-array2
  [^java.io.File file]
  (let [result (byte-array (.length file))]
    (with-open [in (java.io.DataInputStream. (clojure.java.io/input-stream file))]
      (.readFully in result))
    result))

(defn file-to-byte-array [file-name]
  (with-open [mapped-file (mmap/get-mmap file-name)]
    (mmap/get-bytes mapped-file 0 (.size mapped-file))))

;; functions to detect dupes
(defn detect-dupes [file-name]
  (let [data  (file-to-byte-array file-name)
        count (count data)
        max   (IntTools/asInt (.getBytes "ZZZ999") 0)
        bs    (BitSet. max)]
        (loop [n 0]
          (when (< n count)
            (let [val (IntTools/asInt data n)]
              (if (.get bs val)
                (println "Dupe at line" (inc n) "-" (String. ^bytes data n 6))
                (do (.set bs val)
                    (recur (+ n 7)))))))))

(defn -main [& args]
  (when (seq args))
  (let [result (with-out-str
                 (time (detect-dupes (first args))))]
    (println result)))

;(defn detect-dupes-dotimes [file-name]
;  (let [data  (file-to-byte-array (clojure.java.io/file file-name))
;        count (count data)
;        size  (/ count 7)
;        max   (IntTools/asInt (.getBytes "ZZZ999") 0)
;        bs    (BitSet. max)]
;    (try
;      (dotimes [n size]
;        (let [off (* n 7)
;              val (IntTools/asInt data off)]
;          (if (.get bs val)
;            (throw (Exception. (str "Dupe at line " (inc n) " - " (String. ^bytes data off 6))))
;            (.set bs val))))
;      (catch Exception e (.getMessage e)))))
;
;
;(defn detect-dupes-reduce [file-name]
;  (let [data  (file-to-byte-array (clojure.java.io/file file-name))
;        count (count data)
;        max   (IntTools/asInt (.getBytes "ZZZ999") 0)
;        bs    (BitSet. max)]
;    (reduce
;      (fn [bs n]
;        (when (>= n count) (reduced "No dupes!"))
;        (let [val (IntTools/asInt data n)]
;          (if (.get bs val)
;            (reduced (str "Dupe at line " (inc n) " - " (String. ^bytes data n 6)))
;            (do (.set bs val)
;                bs))))
;      bs
;      (range 0 count 7))))
;
;(defn detect-dupes-some [file-name]
;  (let [data  (file-to-byte-array (clojure.java.io/file file-name))
;        count (count data)
;        max   (IntTools/asInt (.getBytes "ZZZ999") 0)
;        bs    (BitSet. max)]
;    (some
;      (fn [n]
;        (when (< n count)
;          (let [val (IntTools/asInt data n)]
;            (if (.get bs val)
;              (str "Dupe at line " (inc n) " - " (String. ^bytes data n 6))
;              (do
;                (.set bs val)
;                false)))))
;      (range 0 count 7))))
