package mxy;

public class IntTools {
  private static int sub =
      65 * 676000 +
          65 * 26000  +
          65 * 1000   +
          48 * 100    +
          48 * 10     +
          48;

  public static int asInt(byte[] arr, int offset) {
    return (arr[offset    ] * 676000)  +
           (arr[offset + 1] * 26000)   +
           (arr[offset + 2] * 1000)    +
           (arr[offset + 3] * 100)     +
           (arr[offset + 4] * 10)      +
           (arr[offset + 5])           -
           sub;
  }

}
