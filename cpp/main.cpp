#include <cstdlib>
#include <cstdio>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <chrono>
#include <memory>
#include <stdexcept>
#include <utility>

namespace
{
    template<typename TOnExit>
    struct scope_guard
    {
        scope_guard (TOnExit const & on_exit)
            : run_on_exit   (true)
            , on_exit       (on_exit)
        {
        }

        scope_guard (TOnExit && on_exit)
            : run_on_exit   (true)
            , on_exit       (std::move (on_exit))
        {
        }

        scope_guard (scope_guard && sc)
            : run_on_exit   (std::move (sc.run_on_exit))
            , on_exit       (std::move (sc.on_exit))
        {
            sc.run_on_exit = false;
        }

        scope_guard & operator= (scope_guard &&)        = delete;

        scope_guard (scope_guard const &)               = delete;
        scope_guard & operator= (scope_guard const &)   = delete;

        ~scope_guard () noexcept
        {
            if (run_on_exit)
            {
                on_exit ();
            }
        }

    private:
        bool    run_on_exit ;
        TOnExit on_exit     ;
    };

    template<typename TOnExit>
    auto on_exit (TOnExit && oe)
    {
        return scope_guard<std::decay_t<TOnExit>> (std::forward<TOnExit> (oe));
    }

    template<typename TAction>
    auto time_in_ms (TAction && action)
    {
        //action (); // Warm-up
        auto b4     = std::chrono::high_resolution_clock::now ();
        action ();
        auto now    = std::chrono::high_resolution_clock::now ();
        auto diff   = now - b4;
        return std::chrono::duration_cast<std::chrono::milliseconds> (diff).count ();
    }

    void find_dupes (char const * file_name)
    {
        auto fd = open (file_name, O_RDONLY);
        if (fd < 0)
        {
            throw std::runtime_error ("Failed to open file");
        }
        auto close_fd = on_exit ([fd] { close (fd); } );

        struct stat st;
        if (fstat (fd, &st) != 0)
        {
            throw std::runtime_error ("Failed to fstat file");
        }

        auto len = static_cast<int> (st.st_size);

        auto addr = mmap (0, len, PROT_READ, MAP_FILE | MAP_PRIVATE, fd, 0);
        if (addr == MAP_FAILED)
        {
            throw std::runtime_error ("Failed to mmap file");
        }
        auto close_addr = on_exit ([addr, len] { munmap (addr, len); } );

        char const * input = static_cast<char const*> (addr);
        char output[8] {};

        std::uint64_t set[(26*26*26*10*10*10) >> 6] {};

        for (int i = 0; i + 5 < len; i += 7)
        {
            auto k =
                    (input[i + 0] - 'A')*26*26*10*10*10
                +   (input[i + 1] - 'A')*26*10*10*10
                +   (input[i + 2] - 'A')*10*10*10
                +   (input[i + 3] - '0')*10*10
                +   (input[i + 4] - '0')*10
                +   (input[i + 5] - '0')*1
                ;
            auto idx    = k >> 6        ;
            auto bit    = k & 0x3F      ;
            auto bits   = set[idx]      ;
            auto test   = 1UL << bit    ;
            auto nbits  = bits|test     ;
            if (nbits != bits)
            {
                set[idx] = nbits;
            }
            else
            {
                std::memcpy (output, &input[i], 6);
                std::printf ("Dupe found: %s\n", output);
                break;
            }
        }
   }

}

int main ()
{
    try
    {
        auto ms = time_in_ms ([] { find_dupes ("../txt/Rgn02.txt"); });

        std::printf ("find_dupes took: %lld ms\n", ms);

        return 0;
    }
    catch (std::exception const & e)
    {
        std::printf ("Caught exception: %s\n", e.what ());
        return 998;
    }
    catch (...)
    {
        std::printf ("Caught unrecognized exception\n");
        return 999;
    }
}