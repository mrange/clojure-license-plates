(defproject clojure-license-plates "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [criterium "0.4.4"]
                 [clj-mmap "1.1.2"]]
  :aot  [clojure-license-plates.core]
  :main clojure-license-plates.core
  :java-source-paths ["src/java"]
  :jvm-opts ["-Xmx1024m" "-Xms1024m" "-server"])
